================
Getting started
================


1. Test File 1 Main
===================

Any text.

.. math::
 x^2+y^2=1
 :label: eq_a

Math block example with label:

.. math::
 e^{i\pi} + 1 = 0
 :label: eq_b


1.1 Some Examples
****************

.. math::
 x^2+y^2=3
 :label: eq_c


2. Test File 2 Main
=============

Refer to :eq:`eq_a`

Refer to :eq:`eq_b`

Refer to :eq:`eq_c`
