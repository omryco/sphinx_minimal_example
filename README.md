# Sphinx Minimal_Example

Website is at https://omrycohen.gitlab.io/sphinx_minimal_example/

## What is going on here?

- Inside the `docs` folder, I have some reStructuredText (`.rst`) files with some content (and equations). 

- I use [Sphinx](https://www.sphinx-doc.org/en/master/) to turn the files in the `docs` folder to HTML files:
  - This is done by calling the `sphinx-build` command inside `.gitlab-ci.yml`. This command runs the `docs/conf.py` file.
  - Sphinx uses MathJax to display equations. 

- I use GitLab Pages service to host the site (this is not imprtant).
